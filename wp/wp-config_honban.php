<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'crecafe-madori_fugakucc');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'crecafe-madori');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'pass1119');

/** MySQL のホスト名 */
define('DB_HOST', 'mysql453.db.sakura.ne.jp');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V(Dae|J>@9[%BH%;?::@/8HOE>)vL]c&?NCa-&SiMj1A71,4#r/<lW+]oz?,+WDT');
define('SECURE_AUTH_KEY',  'H-{L2H7 u>,mPtN6}%hhfQ!%{q/RM?<&7~s[!*;+jPCH]9v)>W$i9!w{z!x=#Q|!');
define('LOGGED_IN_KEY',    'L9}^Zu9oHDQ_76doOSBx$_JJ@7c_w#hR#d |@drqB^z$a5+Tq|IH#-D~lg(H,<dG');
define('NONCE_KEY',        '^~%T7.k7 MvR2<oC`8P%9uSV!$tXU/NXV_u_mC%hj]:#1bS4BJufEuXKxbIUYy9y');
define('AUTH_SALT',        '|ws4|4$Bd] vBfk0 !f.1*5Q|AHZ8S`<}QLM*32xV-|LGw;=(e+Mh ?X6kcJc8!a');
define('SECURE_AUTH_SALT', 'F:r=di,w{DvWl-+i6O`[b P>z+Y5m708LYhh.Nq)+Pdy,Bv[%-l+ZY;9ixKY84-p');
define('LOGGED_IN_SALT',   '0C#}|D!SgiV$7j?o`^9Y(n&VPcWZH)N2Izxi5QXYwRkoRo-=ze1J_>/k7x bO0^7');
define('NONCE_SALT',       'f+@-M CL{QCLm6YnL%L@p Ej,DxnO=XuT#L&cN@~1F|=Lvle@F;W%h5/!-Wdy+$X');

/**#@-*/

// 自動アップデートの停止
define( 'AUTOMATIC_UPDATER_DISABLED', true );

// クッキーパスを変更
define('COOKIEPATH','/');
define('SITECOOKIEPATH','/');
define('ADMIN_COOKIE_PATH','/wp/');

define('WP_MEMORY_LIMIT', '512M');
define('WP_MAX_MEMORY_LIMIT', '512M');

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/** ローカルでのプラグイン更新->FTP設定不要 */
define('FS_METHOD','direct');

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
