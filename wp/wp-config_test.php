<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'crecafe-madori_kabayagarden');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'crecafe-madori');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'pass1119');

/** MySQL のホスト名 */
define('DB_HOST', 'mysql453.db.sakura.ne.jp');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+M|Y.S3lWUk+fq0P+jL@-C6zP-+in3-4<uar;KJ+]Ouo+P?JDI Hv;eH5+O~WLsJ');
define('SECURE_AUTH_KEY',  '}3:+|1;UGJsJ9$6:dE nM!><++mT-~&#Aha@1L2n&RQLU?cNlCuf?l|gz$&H-* <');
define('LOGGED_IN_KEY',    'T>>I@pfhLGmCr_C&.?!1Xwd]{JEVU6X-CB-H-&#eGa>)PYWLryU+Bw@N=Am?hl&!');
define('NONCE_KEY',        '~Q[b=pkx>TIq$J#ne[->Pu$o)vSA-m3 {:eEqtNrl0!g|] Fz|x)>TMXc-.Kl|,>');
define('AUTH_SALT',        '$|#_9$|MZd5u7C;7{bW+@gGQ~7:O`FJoJ}NZ/HT9r,N@O,*|j?ubQ(N7i?;j<M3M');
define('SECURE_AUTH_SALT', '5&DR_oUB(JDy R~kC[Jr~j>D~~d+cvx!U|m7?s<0-k(B|_R!= Pa|+FMXQwDeLsQ');
define('LOGGED_IN_SALT',   '+12.CV$O=F_W)%>Ije+.BjQt+lDRyA>iIB#?z^?{>Clt<,*nCaM $KpZUg5RWSBH');
define('NONCE_SALT',       'F9 N1j>MczlV 00isre`9X,12;8P9DCn?]Q|g5}@IYq)E{u~N@*b3@PQe|o5=+MY');

/**#@-*/

// 自動アップデートの停止
define( 'AUTOMATIC_UPDATER_DISABLED', true );

// クッキーパスを変更
define('COOKIEPATH','/');
define('SITECOOKIEPATH','/');
define('ADMIN_COOKIE_PATH','/wp/');

define('WP_MEMORY_LIMIT', '512M');
define('WP_MAX_MEMORY_LIMIT', '512M');

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/** ローカルでのプラグイン更新->FTP設定不要 */
define('FS_METHOD','direct');

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
