<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'fugakucc_wp');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'root');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'pass');

/** MySQL のホスト名 */
define('DB_HOST', '127.0.0.1');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'w7Uw3~(#4|!9`0|nSc5,/Of6yYh/M+h*]|t|M;K^v7X]fI1GKrx v;Fs#3nvx-Dj');
define('SECURE_AUTH_KEY',  'f3zI]:K88$Z;r6?F]0nU?D+dT]XM$6=yn~L>H0HI4-5qkjW//Uswy^!qJ>v[?tAf');
define('LOGGED_IN_KEY',    'YNyyXYx<G3P.p0iJ8AE_u=y$Y L4y!x:I9SC+A_8C;-C-6{G&-F!hEz.{t8zP:MM');
define('NONCE_KEY',        '`G^jeY=3T<]l+s<?{<sk7K2<0navyBJsX4H1ylui2P5[JzZdR5;*&68-]LN?~5;S');
define('AUTH_SALT',        '%t3}4z5D}&v+e&P(zqSx%V?nV|pJ89|BN#[S3d!I<0dZ_hBgM-C9K9N*Hul|>eIL');
define('SECURE_AUTH_SALT', 'fl^>p.c/H@,uf0m4:867eZmkxLQD|a0y+^.eFxP`c%mcp]E-_eukCnsdZ)RFB%<$');
define('LOGGED_IN_SALT',   'b;tv>< *pDr^u<0-X.|UpWQUjGpB$^m5s|-W(.$W6UB<zx|psR)~KuxIyY3M}dW~');
define('NONCE_SALT',       'l+cmoml-,/S&UF0(YLOhwH+1n8K5F%zK7a++W-%JfS->RP*Eq(x1l~@-1PJPb{0i');

/**#@-*/

// 自動アップデートの停止
define( 'AUTOMATIC_UPDATER_DISABLED', true );

// クッキーパスを変更
define('COOKIEPATH','/');
define('SITECOOKIEPATH','/');
define('ADMIN_COOKIE_PATH','/wp/');

define('WP_MEMORY_LIMIT', '512M');
define('WP_MAX_MEMORY_LIMIT', '512M');

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', true);

/** ローカルでのプラグイン更新->FTP設定不要 */
define('FS_METHOD','direct');

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
