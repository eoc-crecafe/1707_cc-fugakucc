<?php

/**
* Contents
*
 *
 * 顧客向け管理画面表示カスタマイズ
 * - フッターWordPressリンクをカスタマー窓口へ変更
 * - フッターWordPressバージョン情報を削除
 * - メインメニューの表示カスタマイズ
 * - クイック編集の非表示
 * - 投稿一覧の表示項目カスタマイズ
 * - カスタム投稿の一覧へ作成者の項目を追加
 * - 投稿画面の項目を非表示
 * - HTMLエディタの不要機能削除
 * - パーマリンクの非表示にする
 * - ヘッダの不要な項目を削除
 * - ダッシュボードの整理
 * - ダッシュボードにオリジナル内容を設定
 * - CSSで不要項目強制非表示
 * - バージョン更新を非表示にする
 * - メインメニューの並び順を制御
 * - 投稿ページに独自のJSファイルを読み込み
 *
 */


 /**
  * 顧客向け管理画面表示カスタマイズ
  * 【対象】編集者以下
  */


if (!current_user_can('level_10')) {


/**
 * フッターWordPressリンクをカスタマー窓口へ変更
 *
 */
function custom_admin_footer() {
	echo '<a href="http://www.crecafe.co.jp" target="_blank">お問い合わせは株式会社クリエイターズカフェまで</a>';
}
add_filter('admin_footer_text', 'custom_admin_footer');


/**
 * フッターWordPressバージョン情報を削除
 *
 */
function custom_admin_footer_update() {
  remove_filter('update_footer', 'core_update_footer');
}
add_action('admin_menu', 'custom_admin_footer_update');


/**
 * メインメニューの表示カスタマイズ
 *
 */
function remove_menu() {
	remove_menu_page('upload.php'); // メディア
	remove_menu_page('link-manager.php'); // リンク
	remove_menu_page('edit.php?post_type=page'); // 固定ページ
	remove_menu_page('edit-comments.php'); // コメント
	remove_menu_page('themes.php'); // 概観
	remove_menu_page('plugins.php'); // プラグイン
	remove_menu_page('users.php'); // ユーザー
	remove_menu_page('profile.php'); // プロフィール
	remove_menu_page('tools.php'); // ツール
	remove_menu_page('options-general.php'); // 設定
	remove_menu_page('edit.php?post_type=acf-field-group'); // カスタムフィールド
	remove_menu_page('cptui_main_menu'); // CPT UI
  remove_menu_page('wpcf7');

  remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category'); // 投稿 -> カテゴリ
  remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag'); // 投稿 -> タグ
  remove_submenu_page('edit.php', 'edit.php?page=to-interface-post'); // 投稿 -> タグ
}
add_action('admin_menu', 'remove_menu');


/**
 * クイック編集・表示の非表示
 *
 */
add_filter( 'post_row_actions', 'hide_quickedit' );
function hide_quickedit($actions){
	unset($actions['inline hide-if-no-js']);
	unset($actions['view']);
	return $actions;
}


/**
 * 投稿一覧の表示項目カスタマイズ
 *
 */
function custom_columns ($columns) {
	unset($columns['tags']); // タグ、カスタムフィールド
	unset($columns['comments']); // コメント
	return $columns;
}
add_filter('manage_posts_columns', 'custom_columns');


/**
 * 投稿画面の項目を非表示
 *
 */
function remove_default_post_screen_metaboxes() {
  //remove_post_type_support( 'post', 'editor' );  // 本文
  remove_meta_box( 'postcustom','post','normal' ); // カスタムフィールド
  remove_meta_box( 'postexcerpt','post','normal' ); // 抜粋
  remove_meta_box( 'commentstatusdiv','post','normal' ); // ディスカッション
  remove_meta_box( 'commentsdiv','post','normal' ); // コメント
  remove_meta_box( 'trackbacksdiv','post','normal' ); // トラックバック
  remove_meta_box( 'authordiv','post','normal' ); // 作成者
  remove_meta_box( 'slugdiv','post','normal' ); // スラッグ
  remove_meta_box( 'revisionsdiv','post','normal' ); // リビジョン
  remove_meta_box( 'tagsdiv-post_tag' , 'post' , 'normal' ); // 投稿のタグ

  unregister_taxonomy_for_object_type( 'category', 'post' ); // カテゴリ
	unregister_taxonomy_for_object_type( 'post_tag', 'post' ); // タグ

  remove_meta_box( 'slugdiv','blog','normal' ); // スラッグ

 }
add_action('admin_menu','remove_default_post_screen_metaboxes');


/**
 * HTMLエディタの不要機能削除
 *
 */
function custom_htmleditor_settings( $initArray ){
	//不要なボタンを除いた文字列を格納
	$initArray['buttons'] = 'strong,em,link,block,del,ins,img,spell,close,fullscreen';
	//$initArray['buttons'] = ' ';
	return $initArray;
}
add_filter( 'quicktags_settings', 'custom_htmleditor_settings' );


/**
 * ヘッダの不要な項目を削除
 *
 */
function remove_admin_bar_menu( $wp_admin_bar ) {
	$wp_admin_bar->remove_menu('wp-logo'); // WordPressロゴ
	$wp_admin_bar->remove_menu('my-sites'); // 参加サイト for マルチサイト
	//$wp_admin_bar->remove_menu('site-name'); // サイト名
	//$wp_admin_bar->remove_menu('view-site'); // サイト名 -> サイトを表示
	$wp_admin_bar->remove_menu('updates'); // 更新
	$wp_admin_bar->remove_menu('comments'); // コメント
	$wp_admin_bar->remove_menu('new-content'); // 新規
	$wp_admin_bar->remove_menu('new-post'); // 新規 -> 投稿
	$wp_admin_bar->remove_menu('new-media'); // 新規 -> メディア
	$wp_admin_bar->remove_menu('new-link'); // 新規 -> リンク
	$wp_admin_bar->remove_menu('new-page'); // 新規 -> 固定ページ
	$wp_admin_bar->remove_menu('new-user'); // 新規 -> ユーザー
	//$wp_admin_bar->remove_menu('my-account'); // マイアカウント
	//$wp_admin_bar->remove_menu('user-info'); // マイアカウント -> プロフィール
	$wp_admin_bar->remove_menu('edit-profile'); // マイアカウント -> プロフィール編集
	//$wp_admin_bar->remove_menu('logout'); // マイアカウント -> ログアウト
	$wp_admin_bar->remove_menu('search'); // 検索
}
add_action( 'admin_bar_menu', 'remove_admin_bar_menu', 201 );


/**
 * ダッシュボードの整理
 *
 */
function example_remove_dashboard_widgets() {
	global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // 現在の状況
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']); // アクティビティ
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // 最近のコメント
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // 被リンク
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // プラグイン
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // クイック投稿
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // 最近の下書き
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPressブログ
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // WordPressフォーラム
}
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets');


/**
 * ダッシュボードにオリジナル内容を設定
 *
 */

// ようこそ
remove_action( 'welcome_panel', 'wp_welcome_panel' );

// 基本情報
function custom_dashboard_widget_function1() {
	$admin_path = get_admin_url();
echo <<<EOF
  	<div class="eyes-custom-dashboard-widget">
  	<p>編集を開始してください。</p>
  	<ul>
  	<li><a href="{$admin_path}edit.php">&gt;&nbsp;お知らせ</a></li>
  	<li><a href="{$admin_path}edit.php?post_type=competition">&gt;&nbsp;競技結果</a></li>
  	</ul>
  	</div>
EOF;
}

function custom_add_dashboard_widgets1() {
	wp_add_dashboard_widget('custom_dashboard_widget1', '基本情報', 'custom_dashboard_widget_function1');
}
add_action('wp_dashboard_setup', 'custom_add_dashboard_widgets1' );


/**
 * CSSで不要項目強制非表示
 *
 */
function my_admin_head(){
  // ヘルプ
  echo '<style type="text/css">#contextual-help-link-wrap{display:none;}</style>';
  // 表示オプション
  echo '<style type="text/css">#screen-options-link-wrap{display:none;}</style>';
  // カテゴリでの親選択
  echo '<style type="text/css">.form-field.term-parent-wrap{display:none;}</style>';
  // カテゴリでの親選択
  echo '<style type="text/css">.taxonomy-category #the-list .view{display:none;}</style>';
}
add_action('admin_head', 'my_admin_head');


/**
 * バージョン更新を非表示にする
 *
 */
add_filter('pre_site_transient_update_core', '__return_zero');
remove_action('wp_version_check', 'wp_version_check');
remove_action('admin_init', '_maybe_update_core');


/**
 * メインメニューの並び順を制御
 *
 */
function custom_menu_order($menu_ord) {
	if (!$menu_ord) return true;
	return array(
		'index.php', // ダッシュボード
    'separator1', // 区切り線１
		'edit.php', // 投稿
		'edit.php?post_type=schedule', // スケジュール
		'edit.php?post_type=competition', // 競技結果
	);
}
add_filter('custom_menu_order', 'custom_menu_order');
add_filter('menu_order', 'custom_menu_order');


/**
 * 投稿ページに独自のJSファイルを読み込み
 */
function staff_script() {
  global $hook_suffix;

  if('post.php' == $hook_suffix || 'post-new.php' == $hook_suffix) {
    wp_enqueue_script('staff_script', get_stylesheet_directory_uri().'/js/staff_script.js', array('jquery'));
  }
}
add_action('admin_enqueue_scripts', 'staff_script');


} // level_10 end
