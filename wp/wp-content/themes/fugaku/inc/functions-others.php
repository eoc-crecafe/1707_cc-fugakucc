<?php
/**
* Contents
*
* アップロード画像サイズ設定
* アップロード画像名設定
* アーカイブでの表示件数の設定
* postのデフォルトをお知らせに設定
* editor-style.cssの読み込み
* ビジュアルエディタへの挿入画像をfigureタグで囲む
* wpサイトはリダイレクト
* プレビューのURLを置き換え
* パーマリンクの非表示にする
* ビジュアルリッチエディタの不要機能削除
* CSSで不要項目強制非表示
*
*/

/**
* アップロード画像サイズ設定
*
*/
add_action( 'after_setup_theme', 'custom_imgsize_setup' );
function custom_imgsize_setup() {
  add_image_size('competition_l', 800, 1024, false ); // 競技結果用（大）
  add_image_size('competition_m', 370, 264, true ); // 競技結果用（小）
}


/**
* アップロード画像名設定
*
*/
function rename_mediafile($filename) {
	$info = pathinfo($filename);
	$ext  = empty($info['extension']) ? '' : '.' . $info['extension'];
	$filename = strtolower(time().$ext);
  return $filename;
}
add_filter('sanitize_file_name', 'rename_mediafile', 10);


/**
 * アーカイブでの表示件数の設定
 *
 */
function my_pre_get_posts($query) {
  // 競技結果
  if (!is_admin() && $query->is_main_query() && is_post_type_archive('competition')) {
    $query->set('posts_per_page', -1);
  }
}
add_action('pre_get_posts', 'my_pre_get_posts');


/**
* postのデフォルトをお知らせに設定
*
*/
// 投稿にアーカイブ(投稿一覧)を持たせるようにします。
function post_has_archive( $args, $post_type ) {
	if ( 'post' == $post_type ) {
		$labels['name'] = 'お知らせ';
		$args['labels'] = $labels;
		$args['rewrite'] = true;
		$args['has_archive'] = 'info'; // slug名
		$args['description'] = ''; // description名
	}
	return $args;
}
add_filter( 'register_post_type_args', 'post_has_archive', 10, 2 );


/**
 * editor-style.cssの読み込み
 *
 */
add_editor_style("css/editor-style.css");


/**
 * ビジュアルエディタへの挿入画像をfigureタグで囲む
 *
 */
function image_wrap($html, $id, $caption, $title, $align, $url, $size, $alt){
    $html = '<figure>'."\n".$html."\n".'</figure>'."\n";
    return $html;
}
add_filter('image_send_to_editor','image_wrap',10,8);


/**
 * wpサイトはリダイレクト
 *
 */
function ac_auth_redirect() {
 // if ( !is_user_logged_in() ){
    nocache_headers();
		header("HTTP/1.1 302 Moved Temporarily");
		header('Location: http://'.$_SERVER["HTTP_HOST"].'/');
            header("Status: 302 Moved Temporarily");
		exit();
	//}
}
if('wp-login.php' != $pagenow && 'wp-register.php' != $pagenow) {
	add_action('template_redirect', 'ac_auth_redirect');
}


/**
 * プレビューのURLを置き換え
 *
 */
function custom_preview_link ( )  {
 		$requrl = get_permalink();
		$post_type = get_post_type();

    $p = get_the_ID();
    $t = get_the_title();

		if($post_type == 'post'){
			$requrl = str_replace('/wp/', '/info/detail.php', $requrl);
      $requrl .= '&preview=true';
		}
		if($post_type == 'competition'){
      $requrl = "/competition/detail.php?p={$p}&preview=true";
		}

    return $requrl;
 }
add_filter (  'preview_post_link' , 'custom_preview_link'  );


/**
 * パーマリンクの非表示にする
 *
 */
add_filter( 'get_sample_permalink_html', '__return_false' );


/**
 * ビジュアルリッチエディタの不要機能削除
 *
 */
function custom_editor_settings( $initArray ){
  $initArray['height'] = '300px';	//エディタの高さ

	$post_type = get_post_type();

	$initArray['fontsize_formats'] = "10pt 12pt 13pt 15pt 16pt 18pt 20pt 24pt 28pt 32pt";//フォントサイズをpt基準に

	$initArray['toolbar1'] = 'fontsizeselect,bold,underline,forecolor,backcolor,|,link,unlink,|,alignleft,aligncenter,alignright,|,clearboth,|,removeformat,|,undo,redo';
	$initArray['toolbar2'] = '';

	return $initArray;
}
add_filter( 'tiny_mce_before_init', 'custom_editor_settings' );


/**
 * CSSで不要項目強制非表示
 *
 */
function my_style_hide(){
  echo '<style type="text/css">#wpbody #message a { display: none; }</style>';
}
add_action('admin_head', 'my_style_hide');
