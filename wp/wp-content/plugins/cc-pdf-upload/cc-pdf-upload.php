<?php
/*
Plugin Name: CC-PDFアップロード
Description: 競技日程PDFファイルのアップロード
Version:     1.0
*/

add_action( 'admin_menu', 'my_pdf_menu' );

/** メニュー設定 */
function my_pdf_menu() {
 add_menu_page( '競技日程PDFアップロード', '競技日程PDFアップロード', 'moderate_comments', 'my-pdf-options', 'my_pdf_options', 'dashicons-upload' );
}

/** 権限チェック */
function my_pdf_options() {
 if ( !current_user_can( 'moderate_comments' ) )  {
   wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
 }
?>
<div class="wrap">
<h2>競技日程PDFアップロード</h2>
<p>競技日程用にアップロードするPDF（.pdf）ファイルを選択し、「ファイルをアップロード」ボタンをクリックしてください。</p>
<?php

  // POSTデータがあれば処理実行
  if (isset($_POST['submit'])) {
    $flg_file = false;

    // 処理コード 1: 完了　2: アップロードエラー　3: 拡張子エラー

    if(is_uploaded_file($_FILES["pdf"]["tmp_name"][0])){
      //csvかチェック
      if(pathinfo($_FILES["pdf"]["name"][0], PATHINFO_EXTENSION) == "pdf"){
        $file_name = 'pdf_'.date("ymd_His");

        $uploadpath = ABSPATH.'/../competition/pdf/';
        $uploadfile = $uploadpath.$file_name.'.pdf';
        if(!file_exists($uploadpath)){
          mkdir($uploadpath, 0777);
        }
        if(move_uploaded_file($_FILES["pdf"]["tmp_name"][0], $uploadfile)) {
          date_default_timezone_set('Asia/Tokyo');
          update_option('cc_pdf_update', date("Y.m.d H:i:s"));
          update_option('cc_pdf_filename', $file_name);
          $flg_file = 1;
        } else {
          $flg_file = 2;
        }
      } else {
        $flg_file = 3;
      }
    }

    // 更新内容を通知
    if ($flg_file) {
      //チェックメッセージ
      if($flg_file == 1) {
        echo '<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible">';
        echo '<p>競技日程PDFが正常にアップロードできました。</p>';
        echo '</div>';
      } elseif($flg_file == 2) {
        echo '<div id="setting-error-settings_updated" class="error settings-error notice is-dismissible">';
        echo '<p>競技日程PDFが正常にアップロードできませんでした。</p>';
        echo '</div>';
      } elseif($flg_file == 3) {
        echo '<div id="setting-error-settings_updated" class="error settings-error notice is-dismissible">';
        echo '<p>競技日程PDFのファイル形式が不正です。</p>';
        echo '</div>';
      }

    } else {
      // ファイル選択していない場合
      echo '<div id="setting-error-settings_updated" class="error settings-error notice is-dismissible">';
      echo '<p>ファイルが選択されていないか正常にアップロードできませんでした。</p>';
      echo '</div>';
    }
  }

  // 更新日時の表示設定
  if(get_option('cc_pdf_update')) {
    $cc_pdf_update = get_option('cc_pdf_update');
  } else {
    $cc_pdf_update = "----";
  }

?>
<form action="" method="post" enctype="multipart/form-data">
  <div id="custom-csv-upload-menu">
    <div class="menu">
      <h3>競技日程用PDFファイル</h3>
      <p class="date">最終アップロード：<?php echo $cc_pdf_update; ?></p>
      <p class="select-file">自分のコンピュータからファイルを選択する　<input name="pdf[]" type="file" size="60"></p>
    </div>
  </div>
<?php submit_button('　PDFファイルをアップロード　及び　表示を更新する　'); ?>
</form>
</div>
<?php
}

//CSS追加関数
function add_init(){
    wp_register_style('custom-pdf-upload', plugins_url('css/style.css', __FILE__));
    wp_enqueue_style('custom-pdf-upload');
}
add_action('admin_init', 'add_init');
