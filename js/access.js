function initialize() {
	var w = window.innerWidth;
	var zoomratio,icow,icoh;
	if(w>640){
		zoomratio = 17;
		
	}else{
		zoomratio = 15;
	}
  var latlng = new google.maps.LatLng(34.606619,133.951725);
  var myOptions = {
    zoom: zoomratio,
    center: latlng, 
    mapTypeControlOptions: { mapTypeIds: ['accessmap', google.maps.MapTypeId.ROADMAP] },
	scrollwheel: false
  };
  var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
  
	var pointW;
	var pointH;
	if(w>640){
		pointW=108;
		pointH=111;
	}else{
		pointW=54;
		pointH=56;	
	}
   var icon = {
					url: '../img/common/ico_map.png',
					scaledSize : new google.maps.Size(pointW, pointH),
					origin: new google.maps.Point(0,0),
					anchor: new google.maps.Point(pointW/2, pointH)
				};
  var markerOptions = {
    position: latlng,
    map: map,
    icon: icon,
    title: 'カバヤゴルフガーデン'
  };
  var marker = new google.maps.Marker(markerOptions);
 
            var styleOptions = [
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e9e9e9"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
];

  var styleType = new google.maps.StyledMapType(styleOptions);
            map.mapTypes.set('accessmap', styleType);
            map.setMapTypeId('accessmap');
}

window.onload = function() {
  initialize();
}
var resizeTimer;
var interval = Math.floor(1000 / 60 * 10);
 
window.addEventListener('resize', function (event) {
  if (resizeTimer !== false) {
    clearTimeout(resizeTimer);
  }
  resizeTimer = setTimeout(function () {
     initialize();
  }, interval);
});