(function($) {
	
		/* --------------------------------------------------------------------------------------------
		 * map
		 * --------------------------------------------------------------------------------------------*/
		 
		function map(_id,_lat,_lng) {
			//ここにメイン記述
			//初期表示座標
			var bodyName = $('body').get(0).className.split(" ")[0];
			var bodyName2 = $('body').get(0).className.split(" ")[1];
			var lat = _lat;
			var lng = _lng;
			var zoom;
			if(bodyName2 == 'Pc' && bodyName == 'index'){
				lat = 35.170986;
				lng = 138.527680;
			}else if(bodyName2 == 'Pc' && bodyName == 'access'){
				lat = _lat;
				lng = _lng;
			}else{
				lat = 35.165558;
				lng = 138.528496;
			}
			if(bodyName2 == 'Pc'){
				zoom = 15;
			}else{
				zoom = 14;
			}
			
			var secheltLoc = new google.maps.LatLng(lat,lng);
			
			var myMapOptions = {
				center: secheltLoc,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				scrollwheel: false,
				zoom: zoom
			};
			/* スタイル付き地図 */
			var styleOptions = [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ff0000"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#add570"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"on"},{"hue":"#ff0000"}]},{"featureType":"road","elementType":"labels.text","stylers":[{"visibility":"on"},{"hue":"#ff0000"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"},{"hue":"#ff0000"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"lightness":17},{"visibility":"on"},{"color":"#c2c2c2"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"lightness":16},{"hue":"#ff0000"},{"visibility":"on"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a6d0ea"},{"lightness":17}]}];
				
				var theMap = new google.maps.Map(document.getElementById(_id), myMapOptions);	
				var styledMapOptions = { name: 'カスタムマップ' }
				var monoType = new google.maps.StyledMapType(styleOptions, styledMapOptions);
				theMap.mapTypes.set('custom', monoType);
				theMap.setMapTypeId('custom');
				
			appendData();
			
			/*- appendData() ------------------*/
			function appendData(){
			
			//アイコン
			
			var bodyName = $('body').get(0).className.split(" ")[0];
			var bodyName2 = $('body').get(0).className.split(" ")[1];
			var pointW;
			var pointH;
			if(bodyName == 'index'){
				path = './img/common/img_map.png';
			}else{
				path = '../img/common/img_map.png';
			}
			
			if(bodyName2 == 'Pc'){
				pointW = 185;
				pointH = 84;
			}else{
				pointW = 92;
				pointH = 42;
			}
			var image = {
					url: path,
					scaledSize : new google.maps.Size(pointW, pointH),
					origin: new google.maps.Point(0,0),
					anchor: new google.maps.Point(pointW/2, pointH)
				};
				/*
				var shape = {
						coord: [20,0,8,5,0,16,0,23,5,34,15,40,20,50,24,40,36,35,41,19,35,6],
						type: 'poly'
				};
				*/
				
				var lat = _lat;
				var lng = _lng;
				
				var marker = new google.maps.Marker({
					map: theMap,
					position: new google.maps.LatLng(lat, lng),
					visible: true,
					icon:image
					/*shape:shape*/
				});
		
			}
			
		}

	$(function(){
		//コール
		map('gmap', 35.168142, 138.528023);
		$(window).on('resize', function(){
			map('gmap', 35.168142, 138.528023);
		});
	});

})(jQuery);
