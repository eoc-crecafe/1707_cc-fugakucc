(function($) {

$.cclibrary = {
		/* =====================================================
				アルファオーバー
		======================================================*/
		alphaOver: function() {
				if(!navigator.userAgent.match(/(iPhone|iPad|Android)/)){
					//imgに付与
					$(document).on('mouseover','.alover',function(){$(this).fadeTo(1,0.6);});
					$(document).on('mouseout','.alover',function(){$(this).fadeTo(1,1);});

					//boxalpha包括に付与
					$(document).on('mouseenter','.albover',function(){
						$(this).find('img').fadeTo(1,0.6);
						if(!$(this).hasClass('hover')) $(this).addClass('hover');
					});
					$(document).on('mouseleave','.albover',function(){
						$(this).find('img').fadeTo(1,1);
							if($(this).hasClass('hover')) $(this).removeClass('hover');
					});
				}
		},
		/* =====================================================
				ロールオーバー
		======================================================*/
		rollover: function() {
				var image_cache = new Object();
				var image_d = [];
				var image_ov = [];
				$(".rover img,img.rover").each(function(i) {
					var imgsrc = this.src;
					var dot = this.src.lastIndexOf('.');
					var imgsrc_ro = this.src.substr(0, dot) + '_ov' + this.src.substr(dot, 4);
					image_cache[this.imgsrc_d] = new Image();
					image_cache[this.imgsrc_d].imgsrc_d = imgsrc;
					image_d[i] = imgsrc;

					image_cache[this.imgsrc_ro] = new Image();
					image_cache[this.imgsrc_ro].imgsrc_ro = imgsrc_ro;
					image_ov[i] = imgsrc_ro;

				});
				$(document).on('mouseenter','.rover img,img.rover',function(){
					var idx = $(document).find('.rover img,img.rover').index(this);
					this.src = image_ov[idx];
				});
				$(document).on('mouseleave','.rover img,img.rover',function(){
					var idx = $(document).find('.rover img,img.rover').index(this);
					this.src = image_d[idx];
				});
		},
		/* =====================================================
				boxLink　ページ内リンクはリンク動作なし
		======================================================*/
		boxLink: function(options){
				var o = $.extend({
					targetBox: 'boxLink',
					hover: 'hover'
				}, options);
				if(o.targetBox != ""){
						$(document).on('mouseenter','.'+o.targetBox,function(){
							$(this).addClass(o.hover).css({cursor:'pointer'});
						});
						$(document).on('mouseleave','.'+o.targetBox,function(){
							$(this).removeClass(o.hover).css({cursor:'default'});
						});
						$('.'+o.targetBox).click(function () {
							var boxUrl = $(this).find('a').attr('href');
							//#始まりはリンク処理なし
							if (boxUrl.charAt(0) == '#') {
							}else{
								if ($(this).find('a').attr('target') == '_blank') {
												window.open(boxUrl);
								} else window.location = boxUrl;
								return false;
							}
						});

						if(!navigator.userAgent.match(/(iPhone|iPad|Android)/)){
							$(window).unload(function () {
								$(this).removeClass(o.hover).css({cursor:'default'});
							});
						}
				}
		},
		/* =====================================================
				pageScroll　ページスクロール
		======================================================*/
		pageScroll: function(){
			var duration = 300;
			var easing = 'easeOutCubic';
			$(document).on('click','a[href*=#]',function(){
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var $target = $(this.hash);
            $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
						$scroll = $('body,html');
            if ($target.length) {
							$scroll.animate({scrollTop: $target.offset().top}, duration, easing);
                return false;
            }
        }
			});
		},
		/* =====================================================
				画像差し替え（デフォルトスマホ）
				対象img要素にccswitchクラスを付与、CSSでvisibility:hiddenにしておいてください
		======================================================*/
		ccswitch: function(){
			var $obj = $('.ccswitch');
			var pc = '_pc.';
			var sp = '_sp.';

			$obj.each(function(){
				var $this = $(this);
				function imgChange(){
					if($('body').hasClass('Pc')){
						$this.attr('src',$this.attr('src').replace(sp,pc)).css({visibility:'visible'});
					} else if($('body').hasClass('Sp')) {
						$this.attr('src',$this.attr('src').replace(pc,sp)).css({visibility:'visible'});
					}
				}
				$(window).resize(imgChange);
				imgChange();
			});
		},
		// =====================================================
		// scrolltop
		// =====================================================
		scrolltop: function() {
					var topBtn = $('#pagetop');
					topBtn.css({opacity:0});
					$(window).scroll(function () {
									if ($(this).scrollTop() > 100) {
													topBtn.stop().fadeTo(300,1);
									} else {
													topBtn.stop().fadeTo(300,0);
									}
					});
			},
		// =====================================================
		// mobileDevice判定
		// =====================================================
		mobileDevice: function() {
				if(navigator.userAgent.match(/(iPhone|iPad|Android)/)){
					return true;
				}else{
					return false;
				}
		},
		// =====================================================
		// IsIE判定（11以下）
		// =====================================================
		IsIE: function() {
				var isIE = false; // IEか否か
				var version = null; // IEのバージョン
				var ua = navigator.userAgent;
				if( ua.match(/MSIE/) || ua.match(/Trident/) ) {
				    isIE = true;
				    version = ua.match(/(MSIE\s|rv:)([\d\.]+)/)[2];
				}
				if(isIE) $('html').addClass('isIE');
		},
		/* =====================================================
				modeSwitch　Sp or Pc を判定しクラスをbodyに付与
				★ class="spFlg" をpcでdisplay:none、スマホでdisplay:block以外になる要素にフラグとして付けてください。
		======================================================*/
		modeSwitch: function(){
			var $flgObj = $('.spFlg');
			var $body = $('body');
			$(window).bind('resize',switchResize);
			switchResize();
			function switchResize(){
					if($flgObj.css('display') == 'block'){
						$body.addClass('Sp');
						if($body.hasClass('Pc')) $body.removeClass('Pc');
					}else{
						$body.addClass('Pc');
						if($body.hasClass('Sp')) $body.removeClass('Sp');
					}
			}
		},
		/* =====================================================
				telHack
		======================================================*/
		telHack : function(){
			$(document).on('click','.Pc .tellink a',function(e){
				e.preventDefault();
				return false;
			});
		},
		/* =====================================================
				SPページ遷移 pagehide時hover等不要なクラスをリセット
		======================================================*/
		spPageHide : function(){
			if(navigator.userAgent.match(/(iPhone|iPad|Android)/)){
				window.onpagehide = function() {
					//不要クラスを除去
					$('.dialogIsOpen').removeClass('dialogIsOpen');
					$('.hover').removeClass('hover');
					$('#navOverlay').removeAttr('style');
				}
			}
			window.onpageshow = function(event) {
			    if (event.persisted) {
			         window.location.reload();
			     }
			};

		},
		/* =====================================================
				spNav　★必要に応じて有効化・調整を menuオープン時dialogIsOpenクラス
		======================================================*/
		spNav : function(){
			var $win = $(window);
			var $body = $('body');
			var $menuBtn = $('#menuBtn');
			var mode = '';
			if($body.hasClass('Sp')){
				mode = 'Sp';
			}else{
				mode = 'Pc';
			}
			$win.bind('resize',spNavResize);
			spNavResize();
			function spNavResize(){
				if($body.attr('class').indexOf('Sp') > -1){
					if(mode == 'Pc') resizeEvent();
				}else{
					if(mode == 'Sp') resizeEvent();
				}
			}
			function resizeEvent(){
				if($body.hasClass('Sp') && mode == 'Pc'){
					mode = 'Sp';
				}else if($body.hasClass('Pc') && mode == 'Sp'){
					if($body.hasClass('dialogIsOpen')) {
						$body.removeClass('dialogIsOpen');
						$('#navOverlay').css({display:'none'});
					}
					mode = 'Pc';
				}
			}
			$(document).on('click','#navOverlay , #menuBtn',function(e){
				e.preventDefault();
				if($('#navOverlay').is(':animated')) return false;
				$body.toggleClass('dialogIsOpen');
				if($body.hasClass('dialogIsOpen')){
					$('#navOverlay').css({display:'block',opacity:0}).fadeTo(300,1);
				}else{
					$('#navOverlay').fadeTo(300,0,function(){
						$(this).css({display:'none'});
					});
				}
				return false;
			});

			$('#wrapper').append('<div id="navOverlay"></div>');

		},
		/* =====================================================
				scrollFadeIn
		======================================================*/
		scrollFadeIn: function(selector,delay,duration){
			var _delay = delay;
			var _duration = duration;
			var is_iOs = false;
			if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf( 'iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0)  is_iOs=true;

			var allImage = $("img.sc-fade");
			var allImageCount = allImage.length;
			var completeImageCount = 0;

			if(allImageCount > 0){
				for(var i = 0; i < allImageCount; i++){
					$(allImage[i]).bind("load", function(){
						completeImageCount ++;
						if (allImageCount == completeImageCount){
							// 処理
							$(window).trigger("scroll")
						}
					});
				}
				setTimeout(function(){
					$(window).trigger("scroll")
				},2000);
			}else{
				setTimeout(function(){
					$(window).trigger("scroll")
				},300);
			}

			$(selector).each(function(i){
				var $tar = $(this);
				$tar.css({'opacity':0,'visibility':'visible'});


				$(window).scroll(function(){
					show_update($tar);
				});
				if(is_iOs){
				$(window).bind('touchmove',function(){
					show_update($tar);
				});
				}
			});
			function show_update(_elm){
				if(!_elm.hasClass('sc-show')){
					var bottom = $(window).scrollTop() + $(window).height();
					var elT = _elm.offset().top;
					var dif = _elm.height()/2;
					if(dif > 300) dif = 200;
					if(elT < bottom - dif){
						updated(_elm);
					}
				}
			}
			function updated(_elm){


				//通常
					_elm.addClass('sc-show').velocity({
							 opacity:1
					},{duration:_duration,delay:_delay});


			}

		},
		/* =====================================================
				scDelay list要素（ターゲット .sc-target）
		======================================================*/
		scDelay: function(selector){

			$(selector).each(function(i){
				var $tar = $(this);
				$tar.find('.sc-target').css({'visibility':'visible'});

				show_update($tar);
				$(window).scroll(function(){
					show_update($tar);
				});
			});

			function show_update(_elm){
				if(!_elm.hasClass('show')){
					var bottom = $(window).scrollTop() + $(window).height();
					var elT = _elm.offset().top;
					//if(elT < bottom-50){
					var dif = _elm.height()/2;
					if(dif > 300) dif = 200;
					if(elT < bottom - dif){
						updated(_elm);
					}
				}
			}
			function updated(_elm){
				var delay = 0.2;
				var duration = 1;
				if(_elm.hasClass('reverse')){
					var len = _elm.find('.sc-target').length;
					_elm.find('.sc-target').each(function(i){
						len --;
						 $(this).css({
									'transition-delay': "" + len * delay + "s",
									'transition-duration': "" + duration + "s"
							});
					});
				}else{
					_elm.find('.sc-target').each(function(i){
						 $(this).css({
									'transition-delay': "" + i * delay + "s",
									'transition-duration': "" + duration + "s"
							});
					});
				}
				_elm.addClass('show');
			}
		},
		// =====================================================
		// headFixed
		// =====================================================
		headFixed: function() {
			var $header = $('#header');
			var $gnav = $('.navs');
			var tarH = $header.innerHeight();
			var gnavH = $gnav.innerHeight();
			var scT = $(window).scrollTop();
			resizeFunc();
			
			$(window).scroll(function(){
				scrollFunc();
			});
			scrollFunc();
			function scrollFunc(){
				scT = $(window).scrollTop();
				if(tarH + 5 < scT){
					if(!$header.hasClass('fixed')){
						$header.addClass('fixed').css({top:-tarH}).animate({top:-gnavH},300);
						//$header.addClass('fixed').css({top:-tarH}).animate({top:0},300);
					}
				}else if(gnavH > scT){
					$header.removeClass('fixed').removeAttr('style');
				}
			}
			
			$(window).on('resize',resizeFunc);
			function resizeFunc(){
				if($('body').hasClass('Pc')){
						tarH = $header.innerHeight();
						gnavH = $gnav.innerHeight();
						//tarTop = 48;
				}
			}
		},


		spMenu: function() {
			var $body = $('body');
			var $navs = $('.navs');
			var winH = $body.innerHeight();
			$(window).bind('resize',spMenuResize);
			spMenuResize();
			function spMenuResize(){
				winH = $(document).height();
				if($body.hasClass('Sp')){
					$navs.css({height: winH});
				}else if($body.hasClass('Pc')){
					$navs.css({height: 'auto'});
				}
			}
		}

	///////////////////////
	};













/* =====================================================
    DOM readyfunc
======================================================*/
	$(function(){
		$.cclibrary.IsIE();
		$.cclibrary.alphaOver();
		$.cclibrary.rollover();
		$.cclibrary.boxLink();
		$.cclibrary.pageScroll();
		$.cclibrary.modeSwitch();
		$.cclibrary.spPageHide();
		$.cclibrary.telHack();
		$.cclibrary.headFixed();
		$.cclibrary.spNav();
		//$.cclibrary.scrolltop();

		$.cclibrary.scrollFadeIn('.sc-fade',0,1000);
		$.cclibrary.scrollFadeIn('.sc-fade-zoom',0,1000);
		$.cclibrary.scrollFadeIn('.sc-fade-slideup',0,1000);
		$.cclibrary.scrollFadeIn('.sc-fade-slideleft',0,1000);
		$.cclibrary.scrollFadeIn('.sc-fade-slideright',0,1000);
		$.cclibrary.scDelay('.sc-zoom-box');
		$.cclibrary.scDelay('.sc-fade-box');
		$.cclibrary.scDelay('.sc-slideup-box');
		$.cclibrary.scDelay('.sc-slideleft-box');
		
		$(window).on('load', function(){
			$.cclibrary.spMenu();
		});
	});
})(jQuery);

