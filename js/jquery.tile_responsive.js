/**
 * Flatten height same as the highest element for each row.
 *
 * Copyright (c) 2011 Hayato Takenaka
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 * @author: Hayato Takenaka (http://urin.take-uma.net)
 * @version: 0.0.2
 *
 **************************
 **  custom version @cc  **
 **************************
 * Adjust the height depending on the change in font size
 *フラグにbodyにSp、Pcクラス付与があるので注意
 *
**/
;(function($) {
	
	function method(_sel,_pcColumns,_spColumns){
		var tiles, max, c, h, last = _sel.length - 1, s;
		if($body.hasClass('Sp')){
			var _column = _spColumns;
		}else{
			var _column = _pcColumns;
		}
		if(_column == 'none'){
			$(_sel).height('auto');
			return false;
		}else{
			if(!_column) _column = _sel.length;
			_sel.each(function() {
				s = this.style;
				if(s.removeProperty) s.removeProperty("height");
				if(s.removeAttribute) s.removeAttribute("height");
			});
			_sel.each(function(i) {
				c = i % _column;
	      if(c == 0) tiles = [];
				$tile = tiles[c] = $(this);
				h = ($tile.css("box-sizing") == "border-box") ? $tile.outerHeight() : $tile.innerHeight();
				if(c == 0 || h > max) max = h;
				if(i == last || c == _column - 1) {
					$.each(tiles, function() { this.css("height", max); });
				}
			});
		}
	}
	
	function observar(_sel,_pcColumns,_spColumns){
		var h, $e = $('<div>', {text:'A', fontSize:'1em'}).hide().appendTo('body'); 
		setInterval(function() { 
			if(h != $e.css('font-size')) { 
				h = $e.css('font-size'); 
				_sel.css('height', 'auto');
				method(_sel,_pcColumns,_spColumns);
			} 
		}, 2*$.fx.interval); 
	}
	
	function resize(_sel,_pcColumns,_spColumns){
		$(window).bind('resize',function(){
				if(navigator.userAgent.indexOf("MSIE 7") < 0){
					method(_sel,_pcColumns,_spColumns);
				}
		});
	}

	//$(_selector).tile(_pcColmunNum, _spColmunNum);tileしない場合は'none'
	$.fn.tile = function(_pcColumns, _spColumns) {
		sel = this;
		$body = $('body');
		resize(sel,_pcColumns,_spColumns);
		observar(sel,_pcColumns,_spColumns);
		method(sel,_pcColumns,_spColumns);
		$(window).bind('load',function(){
			method(sel,_pcColumns,_spColumns);
		});
	}

	
})(jQuery);
