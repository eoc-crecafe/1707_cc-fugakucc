<?php
include('../lib/logon.php');
if(!logon()) redirect_input('../');

//競技結果情報の取得PGを読み込み
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__)."/../lib/get-comp-detail.php");
?>
<!DOCTYPE html>
<html lang="ja" class="no-js"><!-- InstanceBegin template="/Templates/main.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5RWW3VV');</script>
<!-- End Google Tag Manager -->
<meta charset="UTF-8">

<!-- InstanceBeginEditable name="metaEdit" -->
<title><?php echo $comp_title; ?>｜富嶽カントリークラブ</title>
<meta name="description" content="<?php echo $comp_title; ?>｜豪快に攻める繊細に寄せる、その一つ一つのプレーを優しく富士が見守る。全てのコースより雄大な富士が望め、コースも距離がしっかりあり、豪快なショットでゴルフをお楽しみ頂ける富嶽カントリークラブです。" />
<!-- InstanceEndEditable -->

<meta name="keywords" content="静岡県,ゴルフ場,富嶽カントリークラブ,静岡市" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<meta name="viewport" content="width=device-width,maximum-scale=2.5">
<meta name="format-detection" content="telephone=no,email=no">
<!-- /*== css ==*/ -->
<link rel="stylesheet" href="../css/common.css">
<link rel="stylesheet" href="../css/style.css">
<link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400i" rel="stylesheet">
<!-- InstanceBeginEditable name="cssEdit" -->
<link rel="stylesheet" href="../css/competition.css">
<!-- InstanceEndEditable -->
<!-- googleAnalytics script -->
<!-- //googleAnalytics script -->
<!-- InstanceParam name="body" type="text" value="competition" -->
<!-- InstanceParam name="subbody" type="text" value="competition-detail" -->
<!-- InstanceParam name="page" type="text" value="競技結果" -->
<!-- InstanceParam name="subpage" type="text" value="●●●●●杯" -->
<!-- InstanceParam name="hole" type="boolean" value="false" -->
<!-- InstanceParam name="meta" type="boolean" value="true" -->
<!-- InstanceParam name="pagettl" type="text" value="Competition" -->
<!-- InstanceParam name="pankuzufree" type="boolean" value="true" -->
<!-- InstanceParam name="pankuzu01" type="text" value="" -->
<!-- InstanceParam name="pankuzu02" type="text" value="" -->
<!-- InstanceParam name="responsive" type="boolean" value="true" -->
</head>

<body id="Top" class="competition competition-detail">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RWW3VV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) --> 
<div id="wrapper">
<header id="header">

<h1 id="logo"><a href="../"><img src="../img/common/logo.png" width="258" alt="富嶽カントリークラブ"></a></h1>

<p id="menuBtn" class="pcHide spFlg"><img src="../img/common/btn_menu.png" width="64" height="62" alt="MENU"></p>
<div class="navs">
<nav id="gnav">
<ul class="clearfix wf-Garamond">
<li class="gnav-top"><a href="../index.html"><span>TOP</span><span class="cap yumin">トップ</span></a></li>
<li class="gnav-reserve"><a href="http://www3.golfyoyaku.jp/rsv_sys/tokyo-leisure/fugaku/calendar" target="_blank"><span>RESERVATION</span><span class="cap yumin">予約</span></a></li>
<li class="gnav-course"><a href="../course/"><span>COURSE</span><span class="cap yumin">コース</span></a></li>
<li class="gnav-facility"><a href="../facility/"><span>FACILITY</span><span class="cap yumin">施設</span></a></li>
<li class="gnav-restaurant"><a href="../restaurant/"><span>RESTAURANT</span><span class="cap yumin">レストラン</span></a></li>
<li class="gnav-competition"><a href="../competition/"><span>COMPETITION</span><span class="cap yumin">競技結果</span></a></li>
<li class="gnav-access"><a href="../access/"><span>ACCESS</span><span class="cap yumin">アクセス</span></a></li>
<li class="gnav-info"><a href="../info/"><span>INFORMATION</span><span class="cap yumin">お知らせ</span></a></li>
</ul>
</nav>
<div class="info-wrap">
<p><a href="tel:0544652211"><img src="../img/common/img_tel.png" width="210" alt="0544-65-2211"></a></p>
</div>
</div>
</header>

<article id="content">

<header id="pageHeader">
<div class="pagehead-inner">
<h1 class="ttl01"><span class="wf-Garamond italic">Competition</span><span>競技結果</span></h1>
<figure><img src="../img/competition/bg_pagettl.jpg" alt=""></figure>
</div>
<!-- InstanceBeginEditable name="pankuzuEdit" -->
<div class="pankuzu">
<ol>
<li><a href="../">TOP</a> ＞ </li><li><a href="./">COMPETITION</a> ＞ </li><li><?php echo $comp_title; ?></li>
</ol>
</div>
<!-- InstanceEndEditable -->
</header>



<div id="main">
<!-- InstanceBeginEditable name="mainEdit" -->
<div class="sec-inner clearfix">

<div class="sec-ttl">
<p><time datetime="<?php get_comp_date($comp_date,'Y-m-d'); ?>" class="wf-libre italic"><?php get_comp_date($comp_date,'Y/m/d'); ?></time> <span>開催</span></p>
<h2><?php echo $comp_title; ?></h2>
<!-- //.sec-ttl --></div>

<div class="result">
<section class="sec-result">
<h3 class="ttl02">競技結果</h3>
<?php if(!empty($comp_result) && count($comp_result) > 0) { ?>
<table>
<tr>
<th>順位</th>
<th>プレーヤー</th>
<th>NET</th>
<th>GROSS</th>
</tr>
<?php foreach ($comp_result as $result) { ?>
<tr>
<th><?php echo $result['rank']; ?></th>
<td><?php echo $result['player']; ?></td>
<td><?php echo $result['net']; ?></td>
<td><?php echo $result['gross']; ?></td>
</tr>
<?php } ?>
</table>
<?php }  ?>
<?php if(!empty($comp_pdf)) { ?>
<p class="btn-result btn-pdf btn-eft"><a href="<?php echo $comp_pdf; ?>" target="_blank">全ての競技結果</a></p>
<?php }  ?>
</section>
<?php if(!empty($comp_img1)) { ?>
<section class="sec-rank1">
<h3 class="ttl02">優勝</h3>
<figure><img src="<?php echo $comp_img1; ?>" alt=""></figure>
</section>
<?php } ?>
<?php if(!empty($comp_img2)) { ?>
<section class="sec-rank2">
<h3 class="ttl02">準優勝</h3>
<figure><img src="<?php echo $comp_img2; ?>" alt=""></figure>
</section>
<?php } ?>
<?php if(!empty($comp_img3)) { ?>
<section class="sec-rank3">
<h3 class="ttl02">3位</h3>
<figure><img src="<?php echo $comp_img3; ?>" alt=""></figure>
</section>
<?php } ?>
<p class="btn-bottom btn-eft"><a href="./competition.php">競技結果一覧へ戻る</a></p>

<!-- //.result --></div>
<!-- /.sec-inner --></div>

<!-- InstanceEndEditable -->
<!-- //#main --></div>
<!-- InstanceBeginEditable name="pagerEdit" -->
<ul class="pagination">
<li class="btn-prev wf-Garamond"><a href="../restaurant/">RESTAURANT</a></li>
<li class="btn-next wf-Garamond"><a href="../access/">ACCESS</a></li>
</ul>
<!-- InstanceEndEditable -->
<!-- //#content --></article>

<footer id="footer">
<div class="food-wrap">
<p id="pagetop"><a href="#Top"><img src="../img/common/pagetop.png" width="109" height="109" alt="PAGETOP" class="ophover"></a></p>
<div class="foot-inner">
<div class="fnav-wrap">
<ul id="fnav">
<li>
<ul>
<li><a href="../index.html">トップ</a></li>
<li><a href="http://www3.golfyoyaku.jp/rsv_sys/tokyo-leisure/fugaku/calendar" target="_blank">予約</a></li>
<li><a href="../course/">コース</a></li>
<li><a href="../facility/">施設</a></li>
<li><a href="../restaurant/">レストラン</a></li>
</ul>
</li>
<li>
<ul>
<li><a href="../competition/">競技結果</a></li>
<li><a href="../access/">アクセス</a></li>
<li><a href="../info/">お知らせ</a></li>
<li><a href="../dresscode/">ドレスコード</a></li>
<li><a href="../point/">ポイントカード</a></li>
</ul>
</li>
<li>
<ul>
<li><a href="../regulation/">ゴルフ場利用規約</a></li>
<li><a href="../sitemap/">サイトマップ</a></li>
<li><a href="../privacy/">プライバシーポリシー</a></li>
<li><a href="../policy/">サイトポリシー</a></li>
</ul>
</li>
</ul>
<p class="btn-point wf-Garamond"><a href="../point/">POINT CARD</a></p>
</div>

<div class="address">
<p class="f-logo"><img src="../img/common/logo_f.png" width="286" alt="富嶽カントリークラブ"></p>
<address>
<dl>
<dt>富嶽カントリークラブ</dt>
<dd class="tellink">〒424-0301　静岡県静岡市清水区宍原1783-1<br>
<a href="tel:0544652211">TEL 0544-65-2211</a> 　FAX 0544-65-2210 </dd>
</dl>
</address>
</div>
</div>
</div>
<p id="copyright"><small>Copyright &copy; 2017 富嶽カントリークラブ All Rights Reserved.</small></p>
</footer>
<!-- //#wrapper --></div>
<!-- /*== js ==*/ -->
<script src="../js/modernizr.2.7.1.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="../js/jquery.easing.js"></script>
<!--[if lt IE 9]>
<script src="../js/html5shiv.js"></script>
<![endif]-->
<script src="../js/cclibrary.js"></script>
<!-- InstanceBeginEditable name="FootEdit" -->
<script>
$(function(){
	$('#feed').find('dl dd').each(function(){
		$(this).on('click',function(){
			if($(this).find('.more').css('display') == 'none') {
				location.href = $(this).find('.more').attr('href');
			}
		});
	});

});
</script>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
