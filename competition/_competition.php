<?php
include('../lib/logon.php');
if(!logon()) redirect_input('../');

//競技結果情報の取得PGを読み込み
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__)."/../lib/get-comp-idx.php");
?>
<!DOCTYPE html>
<html lang="ja" class="no-js"><!-- InstanceBegin template="/Templates/main.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5RWW3VV');</script>
<!-- End Google Tag Manager -->
<meta charset="UTF-8">

<!-- InstanceBeginEditable name="metaEdit" -->
<title>競技結果</title>
<meta name="description" content="競技結果" />
<!-- InstanceEndEditable -->

<meta name="keywords" content="静岡県,ゴルフ場,富嶽カントリークラブ,静岡市" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<meta name="viewport" content="width=device-width,maximum-scale=2.5">
<meta name="format-detection" content="telephone=no,email=no">
<!-- /*== css ==*/ -->
<link rel="stylesheet" href="../css/common.css">
<link rel="stylesheet" href="../css/style.css">
<link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400i" rel="stylesheet">
<!-- InstanceBeginEditable name="cssEdit" -->
<link rel="stylesheet" href="../css/competition.css">
<!--<link rel="stylesheet" href="js/bxslider/jquery.bxslider.css">-->
<!-- InstanceEndEditable -->
<!-- googleAnalytics script -->
<!-- //googleAnalytics script -->
<!-- InstanceParam name="body" type="text" value="competition" -->
<!-- InstanceParam name="subbody" type="text" value="" -->
<!-- InstanceParam name="page" type="text" value="競技結果" -->
<!-- InstanceParam name="subpage" type="text" value="" -->
<!-- InstanceParam name="hole" type="boolean" value="false" -->
<!-- InstanceParam name="meta" type="boolean" value="true" -->
<!-- InstanceParam name="pagettl" type="text" value="Competition" -->
<!-- InstanceParam name="pankuzufree" type="boolean" value="true" -->
<!-- InstanceParam name="pankuzu01" type="text" value="" -->
<!-- InstanceParam name="pankuzu02" type="text" value="" -->
<!-- InstanceParam name="responsive" type="boolean" value="true" -->
</head>

<body id="Top" class="competition">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RWW3VV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) --> 
<div id="wrapper">
<header id="header">

<h1 id="logo"><a href="../"><img src="../img/common/logo.png" width="258" alt="富嶽カントリークラブ"></a></h1>

<p id="menuBtn" class="pcHide spFlg"><img src="../img/common/btn_menu.png" width="64" height="62" alt="MENU"></p>
<div class="navs">
<nav id="gnav">
<ul class="clearfix wf-Garamond">
<li class="gnav-top"><a href="../index.html"><span>TOP</span><span class="cap yumin">トップ</span></a></li>
<li class="gnav-reserve"><a href="http://www3.golfyoyaku.jp/rsv_sys/tokyo-leisure/fugaku/calendar" target="_blank"><span>RESERVATION</span><span class="cap yumin">予約</span></a></li>
<li class="gnav-course"><a href="../course/"><span>COURSE</span><span class="cap yumin">コース</span></a></li>
<li class="gnav-facility"><a href="../facility/"><span>FACILITY</span><span class="cap yumin">施設</span></a></li>
<li class="gnav-restaurant"><a href="../restaurant/"><span>RESTAURANT</span><span class="cap yumin">レストラン</span></a></li>
<li class="gnav-competition"><a href="../competition/"><span>COMPETITION</span><span class="cap yumin">競技結果</span></a></li>
<li class="gnav-access"><a href="../access/"><span>ACCESS</span><span class="cap yumin">アクセス</span></a></li>
<li class="gnav-info"><a href="../info/"><span>INFORMATION</span><span class="cap yumin">お知らせ</span></a></li>
</ul>
</nav>
<div class="info-wrap">
<p><a href="tel:0544652211"><img src="../img/common/img_tel.png" width="210" alt="0544-65-2211"></a></p>
</div>
</div>
</header>

<article id="content">

<header id="pageHeader">
<div class="pagehead-inner">
<h1 class="ttl01"><span class="wf-Garamond italic">Competition</span><span>競技結果</span></h1>
<figure><img src="../img/competition/bg_pagettl.jpg" alt=""></figure>
</div>
<!-- InstanceBeginEditable name="pankuzuEdit" -->
<div class="pankuzu">
<ol>
<li><a href="../">TOP</a> ＞ </li><li><a href="#">COMPETITION</a></li>
</ol>
</div>
<!-- InstanceEndEditable -->
</header>



<div id="main">
<!-- InstanceBeginEditable name="mainEdit" -->
<div class="sec-inner clearfix">
<p class="btn-top btn-pdf btn-eft"><a href="#">競技日程はこちら</a></p>
<ul class="btn-year wf-libre italic clearfix">
<?php
foreach ($year_data as $key=>$year) {
	if($key == $current_year) {
?>
<li class="active"><span><?php echo $key; ?></span></li>
	<?php } else { ?>
<li><a href="./competition.php?year=<?php echo $key; ?>"><?php echo $key; ?></a></li>
<?php
	}
}
?>
</ul>

<div id="feed">
<?php
$comp_text = '<p>該当の情報はありませんでした。</p>';
if(!empty($comp_list) && count($comp_list) > 0){
	$comp_text = '';
	foreach($comp_list as $val){
		$comp_text .= '<dt><time datetime="'.get_comp_date($val['date'],'Y-m-d').'">'.get_comp_date($val['date'],'Y/m/d').'</time> <span>開催</span></dt>';
		$comp_text .= '<dd><span>'.$val['title'].'</span><a href="./detail.php?p='.$val['id'].'" class="more">READ MORE</a></dd>';
	}
	$comp_text = str_replace(array("\r", "\n"), '', $comp_text);
	$comp_text = '<dl>'.$comp_text.'</dl>';
}
echo $comp_text;
?>
</div>
<?php echo $pager; ?>

<!-- /.sec-inner --></div>

<!-- InstanceEndEditable -->
<!-- //#main --></div>
<!-- InstanceBeginEditable name="pagerEdit" -->
<ul class="pagination">
<li class="btn-prev wf-Garamond"><a href="../restaurant/">RESTAURANT</a></li>
<li class="btn-next wf-Garamond"><a href="../access/">ACCESS</a></li>
</ul>
<!-- InstanceEndEditable -->
<!-- //#content --></article>

<footer id="footer">
<div class="food-wrap">
<p id="pagetop"><a href="#Top"><img src="../img/common/pagetop.png" width="109" height="109" alt="PAGETOP" class="ophover"></a></p>
<div class="foot-inner">
<div class="fnav-wrap">
<ul id="fnav">
<li>
<ul>
<li><a href="../index.html">トップ</a></li>
<li><a href="http://www3.golfyoyaku.jp/rsv_sys/tokyo-leisure/fugaku/calendar" target="_blank">予約</a></li>
<li><a href="../course/">コース</a></li>
<li><a href="../facility/">施設</a></li>
<li><a href="../restaurant/">レストラン</a></li>
</ul>
</li>
<li>
<ul>
<li><a href="../competition/">競技結果</a></li>
<li><a href="../access/">アクセス</a></li>
<li><a href="../info/">お知らせ</a></li>
<li><a href="../dresscode/">ドレスコード</a></li>
<li><a href="../point/">ポイントカード</a></li>
</ul>
</li>
<li>
<ul>
<li><a href="../regulation/">ゴルフ場利用規約</a></li>
<li><a href="../sitemap/">サイトマップ</a></li>
<li><a href="../privacy/">プライバシーポリシー</a></li>
<li><a href="../policy/">サイトポリシー</a></li>
</ul>
</li>
</ul>
<p class="btn-point wf-Garamond"><a href="../point/">POINT CARD</a></p>
</div>

<div class="address">
<p class="f-logo"><img src="../img/common/logo_f.png" width="286" alt="富嶽カントリークラブ"></p>
<address>
<dl>
<dt>富嶽カントリークラブ</dt>
<dd class="tellink">〒424-0301　静岡県静岡市清水区宍原1783-1<br>
<a href="tel:0544652211">TEL 0544-65-2211</a> 　FAX 0544-65-2210 </dd>
</dl>
</address>
</div>
</div>
</div>
<p id="copyright"><small>Copyright &copy; 2017 富嶽カントリークラブ All Rights Reserved.</small></p>
</footer>
<!-- //#wrapper --></div>
<!-- /*== js ==*/ -->
<script src="../js/modernizr.2.7.1.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="../js/jquery.easing.js"></script>
<!--[if lt IE 9]>
<script src="../js/html5shiv.js"></script>
<![endif]-->
<script src="../js/cclibrary.js"></script>
<!-- InstanceBeginEditable name="FootEdit" -->
<script>
$(function(){
	$('#feed').find('dl dd').each(function(){
		$(this).on('click',function(){
			if($(this).find('.more').css('display') == 'none') {
				location.href = $(this).find('.more').attr('href');
			}
		});
	});

});
</script>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
