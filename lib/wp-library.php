<?php

//リダイレクト
	//$val = currentDirectoryPath
function redirect_url($val){
	header("location: ".$val."/../");
	exit;
}



//日時変換
function exchange_datetime($datetime , $format="Y.m.d") {
	return date($format , strtotime($datetime) );
}


//全角英数字を半角に（記号も半角）
function fnc_convert_text($text){

	$text = mb_convert_kana($text, "a", "UTF-8");

	return $text;
}




/**
 *  総記事数取得
 *  @param  number  $post_type   カスタム投稿タイプ
 *  @param  number  $taxonomy   タクソノミー名
 */
function get_post_cnt($post_type = '',$taxonomy_cat = '',$taxonomy = ''){
    global $wpdb , $post;

		$cnt_sql = "SELECT count(DISTINCT ID)
		FROM $wpdb->posts T1, $wpdb->term_taxonomy T2,
		$wpdb->terms T3,
		$wpdb->term_relationships T4
		WHERE post_status = 'publish'

		";

		if(!empty($post_type)){
			$cnt_sql .= " AND post_type = '$post_type' ";
		}
		if(!empty($taxonomy_cat)){
			$cnt_sql .= " AND T2.taxonomy = '$taxonomy_cat'
			AND T2.term_id = T3.term_id
			AND T3.slug = '$taxonomy' AND T4.object_id = T1.ID
			AND T4.term_taxonomy_id 	= T2.term_taxonomy_id ";
		}

    $numposts = $wpdb->get_var($cnt_sql);

    return $numposts;
}





/**
 *  ページ数を計算
 *  @param  int $cnt  総ページ数
 *  @param  int $num  1ページあたりの表示数
 *  @return int
 */
function fn_getPages($cnt , $num=10){
	if($cnt == 0){
		return 0;
	}else{
		$page_num = floor(($cnt-1)/$num) + 1;
		return $page_num;
	}
}


/**
 *  ページ件数テキスト出力
 *  @param  number  $data_cnt   データ件数
 *  @param  number  $limit      1ページ当たりの表示件数
 *  @param  number  $page       現在のページ数
 *  @param  number  $page_num   総ページ数
 *  @param  string  $param      検索パラメータ
 *  @return string
--------------------------------------------------------------*/
function fn_page_nav_text($data_cnt , $limit , $page , $page_num , $param=""){
	$start = ($page-1)*10 + 1;
	if($page == $page_num){
		$end = $data_cnt - (($page-1)*10);
		$output_txt = $data_cnt.'件中'.$start.'件から'.$end.'件表示中';
	}else{
		$output_txt = $data_cnt.'件中'.$start.'件から10件ずつ表示中';
	}
	return $output_txt;
}


/**
 *  ページング出力
 *  @param  number  $data_cnt   データ件数
 *  @param  number  $limit      1ページ当たりの表示件数
 *  @param  number  $page       現在のページ数
 *  @param  number  $page_num   総ページ数
 *  @param  string  $param      検索パラメータ
 *  @param  string  $customname     カスタムタイプ
 *  @return string
 *　カレントページを中心に前後VIEW_PAGE_MENU_WIDTHページ分の数字を表示
--------------------------------------------------------------*/
define("VIEW_PAGE_MENU_WIDTH", 4);//表示ページ数-1
function fn_page_navigation($data_cnt , $limit , $page , $page_num ,$customname="", $param=""){
	//リンクにパラメータをセット
	if($param){
		$linkparam = "?".$param."&page=";
	}else{
		$linkparam = "?page=";
	}
	//出力用テキスト
  $output_txt = '';
	$prevtext = '';

	if($page_num > 1 && $page <= $page_num){

		if($page != 1){
			//前のページ
			$output_txt .= '<li class="prev"><a href="'.$linkparam.($page - 1).'">Prev</a></li>'."\n";
		}/* else {
			$prevtext .= '<li><span>&lt;</span></li>'."\n";
		}*/


		$lasttext = '';
		$firsttext = '';
		$after = '';
		//ページ番号出力---------------------------------------------------------------------------------------------
		$startPage = 1;
		if($page > VIEW_PAGE_MENU_WIDTH) {
			if(VIEW_PAGE_MENU_WIDTH+1 != $page_num){
				$output_txt.= '<li><span class="more">...</span></li>'."\n";
			}
			$startPage = $page - VIEW_PAGE_MENU_WIDTH;
		}
		$endPage = $startPage + VIEW_PAGE_MENU_WIDTH;
		if($endPage > $page_num) $endPage = $page_num;
		if($page_num > VIEW_PAGE_MENU_WIDTH && $page < ($page_num-1) && $page_num != 3+1) {
			$after= '<li><span class="more">...</span></li>'."\n";
		}
		if($page > VIEW_PAGE_MENU_WIDTH && $page < $page_num) {
			$startPage++;
			$endPage++;
		}
		for($i = $startPage ; $i <= $endPage ; $i++){
			if($page==$i){
				$output_txt.= '<li class="current"><span>'.$i.'</span></li>'."\n";
			}else{
				$output_txt.= '<li><a href="'.$linkparam.$i.'">'.$i.'</a></li>'."\n";
			}
		}
		$output_txt.= $after;

		//ul、前リンク等を足して成形
		$output_txt = $firsttext.$output_txt;
		$output_txt .= $lasttext;

		$output_txt = '<ul>'.$prevtext.$output_txt;

		//ページ番号出力終わり---------------------------------------------------------------------------------------

		if($page != $page_num){
			//次のページ
			$output_txt .= '<li class="next"><a href="'.$linkparam.($page + 1).'">Next</a></li>'."\n";
		}/* else {
			$output_txt .= '<li><span>&gt;</span></li>'."\n";
		}*/

	} else if($page <= $page_num) {
		if($data_cnt > 0) {
			//1ページの場合
			/*
			$output_txt .= '<ul>';
			$output_txt .= '<li class="current"><span>1</span></li>'."\n";
			$output_txt .= '';
			*/
			//$output_txt .= '';
		}
	}
	$output_txt .= '</ul>';

	return $output_txt;

}








//1ページあたりの表示件数
function per_page($page,$page_num,$numposts,$listlimit){
	if($page == $page_num) {
		$item_cnt = $numposts;
	}else{
		$item_cnt = $page * $listlimit;
	}
	$item_cnt = $item_cnt - (5 * ($page - 1));
	return $item_cnt;
}




/*
 * 前後リンクID取得
 * detail.php
 * @param $blog_id   ブログID
 * @param $post_id   記事IP
 * @return string
 */
function get_postID($post_id,$post_type = 'post',$taxonomy_cat = '',$taxonomy = '', $y='', $m=''){
	global $wpdb,$post;
	$sql = "SELECT DISTINCT T1.id
							FROM $wpdb->posts T1, $wpdb->term_taxonomy T2,
							$wpdb->terms T3,
							$wpdb->term_relationships T4
							WHERE post_status = 'publish'
							AND post_type='$post_type'
							";
	if(!empty($taxonomy_cat) && !empty($taxonomy)){
		$sql .= " AND T2.taxonomy = '$taxonomy_cat'
		AND T2.term_id = T3.term_id
		AND T3.slug = '$taxonomy' AND T4.object_id = T1.ID
		AND T4.term_taxonomy_id 	= T2.term_taxonomy_id ";
	}
	if(!empty($y) && !empty($m)){
		if($y) {
				$sql .= " AND YEAR(post_date) = $y";
		}
		if($m) {
				$sql .= " AND MONTH(post_date) = $m";
		}
	}
	$sql .= " ORDER BY post_date ASC ";


	$dbdata = $wpdb->get_results($sql);
	$return_arr = array();
	$hit_flag = 0;
	$prev_id = 0;
	foreach( $dbdata as $key => $row){
		if($hit_flag){
			$return_arr["next"]=$row->id;
			break;
		}
		if($row->id == $post_id){
			if($prev_id != 0){
				$return_arr["prev"]=$prev_id;
			}
			$hit_flag = 1;
		}
		$prev_id = $row->id;
	}
	return $return_arr;
}



/**
 *  Galleryのプレビュー用、自信の記事番号取得
 */
function get_post_num_gallery_preview($post_id){
    global $wpdb , $post;

		$cnt_sql = "SELECT DISTINCT T1.id FROM $wpdb->posts T1 WHERE post_type = 'gallery' AND (post_status = 'publish' OR post_status = 'draft') ORDER BY T1.menu_order ASC";

    $dbdata = $wpdb->get_results($cnt_sql);
    $ids = array();
    foreach($dbdata as $key => $row){
    	$ids[] = $row->id;
    }
    $numposts = array_search($post_id, $ids);
    return $numposts;
}





/*
 * フック　アーカイブに年追加
 *
 */
function add_nen_year_archives( $link_html ) {
		$regex = array (
				"/ title='([\d]{4})'/"  => " title='$1年'",
				"/ ([\d]{4}) /"         => " $1年 ",
				"/>([\d]{4})<\/a>/"        => ">$1年</a>"
		);
		$link_html = preg_replace( array_keys( $regex ), $regex, $link_html );


		return $link_html;
}
add_filter( 'get_archives_link', 'add_nen_year_archives' );


/*
 * カテゴリアーカイブ生成　fromDB
 * @param $catnum int
 * @param $post_type str  カスタム投稿タイプ
 * @param $taxonomy str　カスタムタクソノミ名
 * @param $cc_path   本番・テスト判定用
 * @param $page_str   ページパス
 *
 */
function fn_make_archives_taxonomy_fromdb($catnum,$post_type,$taxonomy, $cc_path,$page_str){
	$return_txt = '';
	$url = 'http://'.$_SERVER["HTTP_HOST"];
	if(!empty($cc_path)) $url .= '/pre';
	$url .= '/'.$page_str;

	//タクソノミのターム一覧を取得
	global $wpdb,$post;
	$sql = "SELECT DISTINCT T1.term_id, T1.name
							FROM $wpdb->terms T1, $wpdb->term_taxonomy T2
							WHERE T2.taxonomy = '$taxonomy'
							AND T2.term_taxonomy_id = T1.term_id
							 ORDER BY T1.term_order ASC;
							";
	$dbdata = $wpdb->get_results($sql);

	if(count($dbdata)){
		foreach($dbdata as $val){
			//対象のtermに記事が登録されているか
			$term = get_term( $val->term_id, $taxonomy );
			if($term->count == 0){
				continue;
			}
			$cls = '';
			if($catnum == $val->term_id) $cls = ' class="current"';
			$caturl = $url.'?cat='.$val->term_id;
			$return_txt .= '<li'.$cls.'><a href="'.$caturl.'">'.$val->name.'</a></li>';
		}
		if(empty($catnum) && !isset($_GET["m"]) ){
			$return_txt = '<ul><li class="current"><a href="../'.$page_str.'">ALL</a></li>'.$return_txt.'</ul>';
		}else{
			$return_txt = '<ul><li><a href="../'.$page_str.'">ALL</a></li>'.$return_txt.'</ul>';
		}
	}
	return $return_txt;
}



/*
 * 月アーカイブ生成　fromDB
 * @param $y int
 * @param $m int
 * @param $post_type str  カスタム投稿タイプ
 * @param $cc_path   本番・テスト判定用
 * @param $page_str   ページパス
 *
 */
function fn_make_archives_fromdb($y, $m,$post_type, $cc_path,$page_str){
	$return_txt = '';
	$url = 'http://'.$_SERVER["HTTP_HOST"];
	if(!empty($cc_path)) $url .= '/pre';
	$url .= '/'.$page_str;

	//アーカイブに該当するものをDBから取る
	global $wpdb,$post;
	$sql = "SELECT
							DATE_FORMAT(post_date, '%Y年%m月') as post_date,
							COUNT(*) as count
							FROM $wpdb->posts
							WHERE post_status = 'publish'
							AND post_type='$post_type'
							GROUP BY DATE_FORMAT(post_date, '%Y%m') ORDER BY post_date DESC;
							";
	$dbdata = $wpdb->get_results($sql);
	if(count($dbdata)){
		$return_txt .= '<ul>';
		foreach($dbdata as $val){
			$str = $val->post_date.'（'.$val->count.'）';
			$num = preg_replace('/[^0-9]/', '', $val->post_date);
			$y_num = substr($num,0,4);
			$m_num = substr($num,4,2);

			$cls = '';
			if($y_num == $y && $m_num == $m) $cls = ' class="current"';
			$dateurl = $url.'?m='.$y_num.$m_num;
			$return_txt .= '<li'.$cls.'><a href="'.$dateurl.'">'.$str.'</a></li>';
		}
		$return_txt .= '</ul>';
	}
	if(empty($return_txt)) $return_txt = '<p class="no-post-msg">記事はありません</p>';
	return $return_txt;
}


/*
 * アーカイブ生成
 * @param $y int     アクティブ年
 * @param $page str  置き換えURL
 * @param $cc_path   本番・テスト判定用
 *
 */

function fn_make_archives($y, $m,  $post_type,$cc_path){

	$list = wp_get_archives(apply_filters('widget_archives_args',array('type' => 'monthly','post_type' => $post_type,'echo' => 0)));
	var_dump($list);
	if($cc_path != ""){
		$list = str_replace("wp/","pre/blog/",$list);
	}else{
		$list = str_replace("wp/","blog/",$list);
	}
	//currentクラスの追加
	if($y != 0 && $m!=0){
		//$list = str_replace(" title='".$y, " class='current' title='".$y,$list);
	}
	return $list;

}


/*
 * 文字列中の数値をひとつなぎにして返す
 * @param $str string  文字列
 *
 */
function num_join($str){
		$num_str ='';
		preg_match_all("/[0-9]+/",mb_convert_kana($str, "a", "UTF-8"),$match);
		if(count($match[0])){
			foreach($match[0] as $val){
				$num_str .=$val;
			}
		}else{
				$num_str =false;
		}
		return $num_str;
}
