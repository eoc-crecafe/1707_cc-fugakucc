<?php
	mb_language( "ja" );
	mb_internal_encoding( "utf8" );

	session_start();

	// 限定ページへのパス
	$private_pass = "competition.php";

	if(!isset($_SESSION["fugaku_cc"]["logon"])) {
		$_SESSION["fugaku_cc"]["logon"] = "";
	}

	if( $_SESSION["fugaku_cc"]["logon"] == "on" ) {
		header("Location: ".$private_pass);
	}

	$post_id = '';
	if(isset($_POST["id"])) {
		$post_id = $_POST["id"];
		//$_POST["id"] == "";
	}
	$post_pass = '';
	if(isset($_POST["pass"])) {
		$post_pass = $_POST["pass"];
		//$_POST["pass"] == "";
	}

	$login = "";
	if(isset($_POST["login"])){ $login = "login"; }

	include("../lib/login.php");

	$id = htmlspecialchars($post_id);
	$pass = htmlspecialchars($post_pass);


	if( $private_array["id"] == $id && $private_array["pass"] == $pass && $login == "login" ) {
		// クッキー情報としてid,passを保存
		$expire = time()+60*60*24*30;

		setcookie("fugaku_cc_id",$id,$expire);
		setcookie("fugaku_cc_pass",$pass,$expire);

		// ログオン状態をセッションに保存
		$_SESSION["fugaku_cc"]["logon"] = "on";
		header("Location: ".$private_pass);
		exit;
	}

	$echo_id = '';
	if(isset($_COOKIE["fugaku_cc_id"])){
		$echo_id = $_COOKIE["fugaku_cc_id"];
	}
	$echo_pass = '';
	if(isset($_COOKIE["fugaku_cc_pass"])){
		$echo_pass = $_COOKIE["fugaku_cc_pass"];
	}

	$message = "";
	if( $login  == "login" ) {
		if( $post_id == "" && $post_pass == "" ) {
			$message = '<p class="error-msg">ID/パスワードを入力してログインしてください。</p>';
		} else {
			$message = '<p class="error-msg">入力されたIDとパスワードではログインできません。<br>ログイン情報をお確かめくださいませ。</p>';
		}
	} else {
		$message = "";
	}
