<?php
/*
* WP読み込み
*/
$_SERVER['REQUEST_URI'] = '/wp/';
require_once(dirname(__FILE__)."/../wp/wp-config.php");
require_once(dirname(__FILE__)."/wp-library.php");

/*----------------------------------------------------------------
	記事ID取得
----------------------------------------------------------------*/
$post_id = false;
if(isset($_GET["p"]) && $_GET["p"] > 0){
	$post_id = $_GET["p"];
}

/*----------------------------------------------------------------
	preview判定
----------------------------------------------------------------*/
$preview_flg = false;
if(isset($_GET["preview"])){
	if($_GET["preview"] == "true" && is_user_logged_in()){
		$preview_flg = true;
	}
}

//格納用
$info_title = '';
$info_detail = array();

/*----------------------------------------------------------------
	データ取得
----------------------------------------------------------------*/
if($preview_flg && !empty($post_id)){
	$query = array(
		'posts_per_page'	=> 1,
		'p'								=> $post_id,
		'post_type'				=> 'post',
		'post_status'			=> array( 'publish','draft','private', 'future' )
	);
} else {
	$query = array(
		'posts_per_page'	=> 1,
		'p'								=> $post_id,
		'post_type'				=> 'post',
		'post_status'			=> array( 'publish' ),
		'orderby'					=> 'date',
		'order'						=> 'DESC'
	);
}

$posts = get_posts($query);

if(count($posts)){

	$post = $posts[0];

	/*----------------------------------------------------------------
	プレビュー表示なら$postをpreviewの$postに変更
	----------------------------------------------------------------*/
	if($preview_flg){
		$preview = wp_get_post_autosave( $post ->ID );
		if(!empty($preview)){
			$post_id = $preview -> ID;
			$post = get_post($post_id);
		}
	}

	$info_date = $post->post_date;
	$info_title = $post->post_title;
	$info_content = $post->post_content;
} else {
	redirect_url('/info/');
}

/* meta出力 */
function get_meta_title($is_index,$info_title) {
	$meta_t = '';
	$info_title = str_replace('[br]', '', $info_title);
	if(!$is_index) {
		$meta_t = $info_title.'｜';
	}

	echo $meta_t;
}

/* 日付出力 */
function get_info_date($info_date,$format = 'Y/m/d') {
	if($info_date) {
		echo date($format,strtotime($info_date));
	} else {
		echo '';
	}
}

/* タイトル出力-改行コード変換 */
function get_info_title($info_title) {
	if($info_title) {
		echo str_replace('[br]', '<br>', $info_title);
	} else {
		echo '';
	}
}
/* タイトル出力-改行コード削除 */
function get_info_title2($info_title) {
	if($info_title) {
		echo str_replace('[br]', '', $info_title);
	} else {
		echo '';
	}
}

/* 内容出力 */
function get_info_content($info_content) {
	if($info_content) {
		echo do_shortcode(wpautop($info_content));
	} else {
		echo '';
	}
}
