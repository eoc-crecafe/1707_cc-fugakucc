<?php
mb_language( "ja" );
mb_internal_encoding( "utf8" );
session_start();

function logon() {
	// ログイン状態確認
	if(!isset($_SESSION["fugaku_cc"]["logon"])) {
		$_SESSION["fugaku_cc"]["logon"] = "";
	}
	if( $_SESSION["fugaku_cc"]["logon"] != "on" ) {
		session_destroy();
		return(false);
		exit();
	}
	return(true);
}

function redirect_input($_path) {
	// input画面に戻す
	header("Location: ".$_path."competition/");
	exit();
}
