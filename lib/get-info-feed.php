<?php
/*
* WP読み込み
*/
$_SERVER['REQUEST_URI'] = '/wp/';
require_once(dirname(__FILE__)."/../wp/wp-config.php");

/*----------------------------------------------------------------
	表示する記事数を設定
----------------------------------------------------------------*/
define("NEWS_PER_FEED_NUM", 5);


// 格納用
$info_detail = array();

/* query */
$query = array(
	'posts_per_page'	=> NEWS_PER_FEED_NUM,
	'post_type'				=> 'post',
	'post_status'			=> array( 'publish'),
	'orderby'					=> 'date',
	'order'						=> 'DESC'
);
$posts = get_posts($query);

/*
print_r('<pre>');
var_dump($posts);
print_r('</pre>');
*/

// 必要情報のみを配列に格納
if(count($posts)){
	foreach($posts as $post){
		$info_detail[] = array(
			"id"				=> $post -> ID,
			"date"			=> date("Y.m.d",strtotime($post->post_date)),
			"ttl"				=> str_replace('[br]', '', $post -> post_title)
		);
	}
}

// 表示用に情報を整形
$info_text = '';
if(count($info_detail) && is_array($info_detail)){
	foreach($info_detail as $val){
		$info_text .= '<dt>'.$val['date'].'</dt>';
		$info_text .= '<dd><span>'.$val['ttl'].'</span><a href="info/detail.php?p='.$val['id'].'" class="more">Read more</a></dd>';
	}
	$info_text = str_replace(array("\r", "\n"), '', $info_text);
	$info_text = '<dl>'.$info_text.'</dl>';
}else{
	$info_text = '<p class="no-post-msg">お知らせは現在ありません</p>';
}

//return $info_text;
// JSでHTMLを出力
echo "\$(function(){\$('#feed').find('.info-data').css({display:'block',opacity:0}).html('{$info_text}');\$(window).load(function() {\$('#feed').find('.info-loader').delay(300).fadeTo(200,0,function(){\$(this).remove();\$('#feed').find('.info-data').delay(400).fadeTo(200,1);});});});";

/*
print_r('<pre>');
var_dump($info_detail);
print_r('</pre>');
*/
