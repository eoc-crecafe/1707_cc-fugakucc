<?php
/*
* WP読み込み
*/
$_SERVER['REQUEST_URI'] = '/wp/';
require_once(dirname(__FILE__)."/../wp/wp-config.php");
require_once(dirname(__FILE__)."/wp-library.php");

/*----------------------------------------------------------------
	1Pごとの記事数を設定
----------------------------------------------------------------*/
define("INFO_PER_PAGE_NUM", 10);

/*----------------------------------------------------------------
	記事ID取得
----------------------------------------------------------------*/
$is_index = true;

/* query */
$pageparam = '';

/*----------------------------------------------------------------
	page取得
----------------------------------------------------------------*/
$page = 1;
if(isset($_GET["page"])){
	$page = $_GET["page"];
}

//総記事数取得
$numposts = get_post_cnt('post');
//総ページ数取得
$page_num = fn_getPages($numposts , INFO_PER_PAGE_NUM);
// ページャ作成

$pager = fn_page_navigation($numposts, INFO_PER_PAGE_NUM, $page, $page_num, 'post' ,$pageparam);
if(!empty($pager)) $pager = '<div class="pagenation">'.$pager.'</div>';

//pageparamにpage番号追加
if($page > 1){
	if(!empty($pageparam)){
		$pageparam .= '&page='.$page;
	}else{
		$pageparam .= 'page='.$page;
	}
}

//query
$query = array(
	'posts_per_page' => INFO_PER_PAGE_NUM,
	'post_type' => 'post' ,
	'paged'    => $page,
	'post_status' => array( 'publish'),
	'orderby'        => 'date',
	'order'          => 'DESC'
);

$posts = get_posts($query);

// 必要情報のみを配列に格納
if(count($posts)){
	foreach($posts as $post){
		$info_list[] = array(
			"id"				=> $post -> ID,
			"date"			=> date("Y/m/d",strtotime($post->post_date)),
			"ttl"				=> str_replace('[br]', '', $post -> post_title)
		);
	}
}
