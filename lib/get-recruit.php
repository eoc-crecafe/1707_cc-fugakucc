<?php
/*
* WP読み込み
*/
$_SERVER['REQUEST_URI'] = '/wp/';
require_once(dirname(__FILE__)."/../wp/wp-config.php");


/*----------------------------------------------------------------
	記事ID取得
----------------------------------------------------------------*/
if(isset($_GET["p"]) && $_GET["p"] > 0){
	$post_id = $_GET["p"];
} else {
	// ID指定無し
	$post_id = false;
}

/*----------------------------------------------------------------
	preview判定
----------------------------------------------------------------*/
$preview_flg = false;
if(isset($_GET["preview"])){
	if($_GET["preview"] == "true" && is_user_logged_in()){
		$preview_flg = true;
	}
}

// データ読み込み→previewかどうかで読み込む範囲を変更
$query = array(
	'posts_per_page' => -1,
	'post_type' => 'recruit',
	'post_status' => array( 'publish','draft','private' ),
	'orderby'        => 'menu_order',
	'order'          => 'ASC',
);


$posts = get_posts($query);

$recruit_data = array();

if(count($posts)){
	$i = 0;
	foreach($posts as $post){

		// プレビューの場合で該当記事で自動保存情報があれば$previewと差替え
		if($preview_flg && $post ->ID == $post_id){
			$preview = wp_get_post_autosave( $post ->ID );
			if(!empty($preview)){
				$post_id = $preview -> ID;
				$post = get_post($post_id);
			}
		} elseif($post -> post_status != 'publish') {
			// プレビュー関係無く、公開でない記事は非表示
			continue;
		}

		// 掲載終了はスキップ
		$end = get_field('rec-end',$post->ID);
		if(!empty($end)) { continue; }

		$stop = get_field('rec-stop',$post->ID);
		$recruit_data[$i]['stop'] = empty($stop) ? false : true;
		$recruit_data[$i]['ttl'] = $post->post_title;
		$recruit_data[$i]['cap'] = get_field('rec-caption',$post->ID);
		$recruit_data[$i]['content'] = $txt_group = get_field('rec-content');
		$recruit_data[$i]['tel'] = get_field('rec-tel',$post->ID);
		$recruit_data[$i]['name'] = get_field('rec-name',$post->ID);

		$i++;
	}
}
