<?php
/*
* WP読み込み
*/
$_SERVER['REQUEST_URI'] = '/wp/';
require_once(dirname(__FILE__)."/../wp/wp-config.php");
require_once(dirname(__FILE__)."/wp-library.php");

/*----------------------------------------------------------------
	1Pごとの記事数を設定
----------------------------------------------------------------*/
define("COMP_PER_PAGE_NUM", 10);

/*----------------------------------------------------------------
	記事ID取得
----------------------------------------------------------------*/
$is_index = true;

/* query */
$pageparam = '';

/*----------------------------------------------------------------
	page取得
----------------------------------------------------------------*/
$page = 1;
if(isset($_GET["page"])){
	$page = $_GET["page"];
}

/*----------------------------------------------------------------
	表示年取得 - GET or 今年
----------------------------------------------------------------*/
$current_year = date('Y');
if(isset($_GET["year"])){
	$current_year = $_GET["year"];
}

/*----------------------------------------------------------------
	全データを取得し西暦ごとのデータ有無チェック
----------------------------------------------------------------*/
$query = array(
	'posts_per_page' => -1,
	'post_type' => 'competition' ,
	'post_status' => array( 'publish'),
	'orderby'        => 'date',
	'order'          => 'DESC'
);
$posts = get_posts($query);
$first_flg = false;
foreach ($posts as $post_data) {
	$year = substr($post_data->post_date, 0, 4);
	$year_data[$year][] = array('id'=>$post_data->ID,'title'=>$post_data->post_title,'date'=>$post_data->post_date);
	if(!$first_flg) {
		$first_year = $year;
		$first_flg = true;
	}
}

// GETで取得した西暦及び今年の西暦でデータがなければ、最新記事の西暦を表示年に設定
if(empty($year_data[$current_year])) {
	$current_year = $first_year;
}
//総記事数取得
$numposts = count($year_data[$current_year]);
//総ページ数取得
$page_num = fn_getPages($numposts, COMP_PER_PAGE_NUM);
//pageparamに西暦追加
if(isset($_GET["year"])){
	$pageparam .= 'year='.$_GET["year"];
}
// ページャ作成
$pager = fn_page_navigation($numposts, COMP_PER_PAGE_NUM, $page, $page_num, 'competition' ,$pageparam);
if(!empty($pager)) $pager = '<div class="pagenation">'.$pager.'</div>';

if(!empty($year_data[$current_year]) && count($year_data[$current_year]) > 0) {
	$i = 1;
	$start_i = ($page - 1) * COMP_PER_PAGE_NUM + 1;
	$end_i = $page * COMP_PER_PAGE_NUM;
	foreach ($year_data[$current_year] as $key => $value) {
		if($start_i <= $i && $end_i >= $i) {
			$comp_list[] = $value;
		}
		$i++;
	}
} else {
	$comp_list = false;
}

/* 日付出力 */
function get_comp_date($comp_date,$format = 'Y/m/d') {
	if($comp_date) {
		return date($format,strtotime($comp_date));
	} else {
		return '';
	}
}
