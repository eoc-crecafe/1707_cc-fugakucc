<?php
/*
* WP読み込み
*/
$_SERVER['REQUEST_URI'] = '/wp/';
require_once(dirname(__FILE__)."/../wp/wp-config.php");
require_once(dirname(__FILE__)."/wp-library.php");

/*----------------------------------------------------------------
	記事ID取得
----------------------------------------------------------------*/
$post_id = false;
if(isset($_GET["p"]) && $_GET["p"] > 0){
	$post_id = $_GET["p"];
}

/*----------------------------------------------------------------
	preview判定
----------------------------------------------------------------*/
$preview_flg = false;
if(isset($_GET["preview"])){
	if($_GET["preview"] == "true" && is_user_logged_in()){
		$preview_flg = true;
	}
}

//格納用
$comp_title = '';
$comp_detail = array();

/*----------------------------------------------------------------
	データ取得
----------------------------------------------------------------*/
if($preview_flg && !empty($post_id)){
	$query = array(
		'posts_per_page'	=> 1,
		'p'								=> $post_id,
		'post_type'				=> 'competition',
		'post_status'			=> array( 'publish','draft','private', 'future' )
	);
} else {
	$query = array(
		'posts_per_page'	=> 1,
		'p'								=> $post_id,
		'post_type'				=> 'competition',
		'post_status'			=> array( 'publish' ),
		'orderby'					=> 'date',
		'order'						=> 'DESC'
	);
}

$posts = get_posts($query);

if(count($posts)){

	$post = $posts[0];

	/*----------------------------------------------------------------
	プレビュー表示なら$postをpreviewの$postに変更
	----------------------------------------------------------------*/
	if($preview_flg){
		$preview = wp_get_post_autosave( $post ->ID );
		if(!empty($preview)){
			$post_id = $preview -> ID;
			$post = get_post($post_id);
		}
	}

	$comp_date = $post->post_date;
	$comp_title = $post->post_title;

	// カスタムフィールドのデータ取得
	// 結果
	$comp_result = get_field('comp-result',$post->ID);

	// PDF
	$comp_pdf = get_field('comp-pdf',$post->ID);

	// 画像
	$comp_img1 = get_field('comp-img1',$post->ID);
	$comp_img2 = get_field('comp-img2',$post->ID);
	$comp_img3 = get_field('comp-img3',$post->ID);
	$comp_img = isset($comp_img1['sizes']['competition_l']) && isset($comp_img2['sizes']['competition_m']) && isset($comp_img3['sizes']['competition_m']) ? true : false;

	$comp_img1 = isset($comp_img1['sizes']['competition_l']) ? $comp_img1['sizes']['competition_l'] : false;;
	$comp_img2 = isset($comp_img2['sizes']['competition_m']) ? $comp_img2['sizes']['competition_m'] : false;;
	$comp_img3 = isset($comp_img3['sizes']['competition_m']) ? $comp_img3['sizes']['competition_m'] : false;;
} else {
	redirect_url('./competition.php');
}


/* 日付出力 */
function get_comp_date($comp_date,$format = 'Y/m/d') {
	if($comp_date) {
		echo date($format,strtotime($comp_date));
	} else {
		echo '';
	}
}
